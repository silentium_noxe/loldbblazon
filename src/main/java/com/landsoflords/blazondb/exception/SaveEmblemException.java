package com.landsoflords.blazondb.exception;

public class SaveEmblemException extends SaveException {
    public SaveEmblemException() {
        super();
    }

    public SaveEmblemException(String s) {
        super(s);
    }
}
