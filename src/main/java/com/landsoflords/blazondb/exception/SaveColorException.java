package com.landsoflords.blazondb.exception;

public class SaveColorException extends SaveException {
    public SaveColorException() {
        super();
    }

    public SaveColorException(String s) {
        super(s);
    }
}
