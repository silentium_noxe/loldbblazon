package com.landsoflords.blazondb.exception;

public class SaveException extends Exception {
    public SaveException() {
        super();
    }

    public SaveException(String s) {
        super(s);
    }
}
