package com.landsoflords.blazondb.exception;

public class SaveBlazonException extends SaveException {
    public SaveBlazonException() {
        super();
    }

    public SaveBlazonException(String s) {
        super(s);
    }
}
