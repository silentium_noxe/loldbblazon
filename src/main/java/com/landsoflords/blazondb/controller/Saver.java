package com.landsoflords.blazondb.controller;

import com.landsoflords.blazondb.exception.SaveException;
import com.landsoflords.blazondb.repository.BlazonRepository;
import com.landsoflords.blazondb.repository.model.Blazon;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Component
@RestController
public class Saver extends Controller {

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @PostMapping(value = "/save")
    public ResponseEntity<?> execute(
            @RequestParam("description") String description,
            @RequestParam("sourceId") String sourceId,
            @RequestParam("code") String code,
            HttpServletRequest request) {
        HashMap<String, String> params = new HashMap<>();
        params.put("description", description);
        params.put("sourceId", sourceId);
        params.put("code", code);
        params.put("USER", request.getRemoteUser());
        params.put("USER_ADDRESS", request.getRemoteAddr());

        registerCall(this.getClass(), "execute", params);

        try {
            Blazon blazon = new Blazon(description, code, sourceId);
            BlazonRepository repository = new BlazonRepository();
            repository.save(blazon);

            if(repository.getIdLastSavedBlazon() == null){
                throw new SaveException("Id saved blazon is null");
            }

            logDebug("Id new blazon: "+repository.getIdLastSavedBlazon());
        }catch (Exception e){
            e.printStackTrace();
            logInfo("Controller [Saver] method [execute] end with status: FAIL");
            return new ResponseEntity<>("FAIL", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logInfo("Controller [Saver] method [execute] end with status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
