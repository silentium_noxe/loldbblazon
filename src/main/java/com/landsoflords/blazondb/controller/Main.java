package com.landsoflords.blazondb.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Component
@RestController
public class Main extends Controller {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<String> mainPage(HttpServletRequest request){
        HashMap<String, String> params = new HashMap<>();
        params.put("USER", request.getRemoteUser());
        params.put("USER_ADDRESS", request.getRemoteAddr());

        logInfo("[USER] "+request.getRemoteAddr());

        registerCall(this.getClass(), "mainPage", params);

        return ResponseEntity.ok().header("Access-Control-Allow-Origin", "https://www.landsoflords.com").body("javascript:alert('HELLO');");
    }
}
