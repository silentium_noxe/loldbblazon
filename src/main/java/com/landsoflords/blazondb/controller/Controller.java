package com.landsoflords.blazondb.controller;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
public abstract class Controller {

    static void logInfo(String msg){
        log.info(msg);
    }

    static void logDebug(String msg){
        log.debug(msg);
    }

    protected static void registerCall(Class controller, String method, Map<String, String> params) {
        final String CLASS_NAME = "["+controller.getSimpleName()+"]";
        final String METHOD_NAME = "["+method+"]";

        logInfo("Start controller "+CLASS_NAME+" method "+METHOD_NAME);

        params.forEach((key, value) -> logDebug("Param "+CLASS_NAME+": _"+key+"_ -> "+value));
    }
}
