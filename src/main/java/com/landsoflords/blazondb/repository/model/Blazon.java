package com.landsoflords.blazondb.repository.model;

import com.landsoflords.blazondb.repository.ColorRepository;
import com.landsoflords.blazondb.repository.EmblemRepository;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "blazon")
@ToString
@EqualsAndHashCode
public class Blazon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "backgroundStyle")
    private String backgroundStyle = "";

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Setter(value = AccessLevel.NONE)
    @ElementCollection
    @CollectionTable(name = "blazonColor", joinColumns = @JoinColumn(name = "blazonId"))
    @Column(name = "colorId")
    private Set<Long> colors = new HashSet<>();

    @Setter(value = AccessLevel.NONE)
    @ElementCollection
    @CollectionTable(name = "blazonEmblem", joinColumns = @JoinColumn(name = "blazonId"))
    @Column(name = "emblemId")
    private Set<Long> emblems = new HashSet<>();

    @Setter(value = AccessLevel.NONE)
    @ElementCollection
    @CollectionTable(name = "blazonSource", joinColumns = @JoinColumn(name = "blazonId"))
    @Column(name = "sourceId")
    private Set<String> sourceIds = new HashSet<>();

    public Blazon() {
    }

    public Blazon(String description, String url,String sourceId) throws Exception {
        addSource(sourceId);
        setCode(url);
        setDescription(description);
        parseDescription(description);
    }

    public void addColor(Color color){
        colors.add(color.getId());
    }

    public void removeColor(Color color){
        colors.remove(color.getId());
    }

    public void addEmblem(Emblem emblem){
        emblems.add(emblem.getId());
    }

    public void removeEmblem(Emblem emblem){
        emblems.remove(emblem.getId());
    }

    public void addSource(String id){
        sourceIds.add(id);
    }

    public void removeSource(String id){
        sourceIds.add(id);
    }

    private void parseDescription(String description) throws Exception {
        List<String> banWords = List.of(
                "a", "and", "in", "on", "under", "I", "II", "III", "IV"
        );

        String[] arr = description.split(",");

        for (String item : arr[0].replace(", ", "").trim().split(" ")){
            if (banWords.contains(item)){
                continue;
            }

            if (!item.equals(item.toLowerCase())){
                addColor(new ColorRepository().getExistOrSave(item));
                continue;
            }

            backgroundStyle = backgroundStyle.concat(" "+item);
        }
        backgroundStyle = backgroundStyle.trim();

        EmblemRepository emblemRepository = new EmblemRepository();
        for(int i = 1; i < arr.length; i++){
            Emblem emblem = emblemRepository.get(arr[i].trim());

            if (emblem == null){
                emblem = new Emblem();
                emblem.setDescription(arr[i].trim());
            }else{
                addEmblem(emblem);
                continue;
            }

            for (String item : arr[i].trim().replace("  ", " ").split(" ")){
                if (banWords.contains(item)){
                    continue;
                }

                if (!item.equals(item.toLowerCase())){
                    emblem.setColor(new ColorRepository().getExistOrSave(item));
                    continue;
                }

                if (List.of("two", "three", "four").contains(item)){
                    emblem.setQuantity(item);
                    continue;
                }

                //remove last char "s" in words like peopleS or thingS
                if (emblem.getQuantity() != null && item.endsWith("s")){
                    item = item.substring(0, item.length() -1);
                }

                emblem.addTag(item);
            }
            emblemRepository.save(emblem);
            addEmblem(emblemRepository.get(emblemRepository.getIdLastSavedEmblem()));
        }
    }
}