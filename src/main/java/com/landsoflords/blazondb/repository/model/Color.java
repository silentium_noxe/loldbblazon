package com.landsoflords.blazondb.repository.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "color")
@ToString
@EqualsAndHashCode
public class Color {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    public Color(){

    }

    public Color(String name) {
        this.name = name;
    }
}
