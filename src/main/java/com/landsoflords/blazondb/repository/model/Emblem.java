package com.landsoflords.blazondb.repository.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "emblem")
@ToString
@EqualsAndHashCode
public class Emblem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter(value = AccessLevel.NONE)
    @ElementCollection
    @CollectionTable(name = "emblemTag", joinColumns = @JoinColumn(name = "emblemId"))
    @Column(name = "tag")
    private List<String> tags = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "colorId")
    private Color color;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "description")
    private String description;

    public void addTag(String tag){
        tags.add(tag);
    }

    public void removeTag(String tag){
        tags.remove(tag);
    }
}
