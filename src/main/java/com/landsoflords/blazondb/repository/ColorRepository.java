package com.landsoflords.blazondb.repository;

import com.landsoflords.blazondb.DataBaseConnection;
import com.landsoflords.blazondb.exception.SaveColorException;
import com.landsoflords.blazondb.repository.model.Color;
import lombok.Getter;
import org.hibernate.Session;

import java.util.List;

public class ColorRepository {

    @Getter
    private Long idLastSavedColor;

    public void save(String item) throws SaveColorException {
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            session.beginTransaction();
            Long id = (Long) session.save(new Color(item));
            session.getTransaction().commit();
            idLastSavedColor = id;
        }catch(Exception e){
            if(session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            throw new SaveColorException(e.getMessage());
        }finally{
            session.close();
        }
    }

    public void remove(Color color){
        //TODO:implements
    }

    public List<Color> get(){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            return session.createQuery("FROM Color")
                    .list();
        }finally{
            session.close();
        }
    }

    public List<Color> get(final long id){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            return session.createQuery("FROM Color WHERE id = :val ")
                    .setParameter("val", id)
                    .list();
        }finally{
            session.close();
        }
    }

    public List<Color> get(final String name){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            return session.createQuery("FROM Color WHERE name = :val ")
                                .setParameter("val", name)
                                .list();
        }finally{
            session.close();
        }
    }

    public Color getExistOrSave(final String name) throws SaveColorException{
        List<Color> list = get(name);
        if(list.isEmpty()){
            save(name);
            return get(name).get(0);
        }

        return list.get(0);
    }
}
