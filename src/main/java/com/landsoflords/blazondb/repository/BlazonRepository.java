package com.landsoflords.blazondb.repository;

import com.landsoflords.blazondb.DataBaseConnection;
import com.landsoflords.blazondb.exception.SaveBlazonException;
import com.landsoflords.blazondb.exception.SaveException;
import com.landsoflords.blazondb.repository.model.Blazon;
import lombok.Getter;
import org.hibernate.Session;

import java.util.List;

public class BlazonRepository {

    @Getter
    private Long idLastSavedBlazon;

    public void save(final Blazon blazon) throws SaveException {
        Session session = DataBaseConnection.getSessionFactory().openSession();
        try{
            session.clear();
            session.beginTransaction();
            System.err.println(blazon);
            Long id = (Long) session.save(blazon);
            session.getTransaction().commit();
            idLastSavedBlazon = id;
        }catch(Exception e){
            if(session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            throw new SaveBlazonException(e.getMessage());
        }finally{
            session.close();
        }
    }

    public Blazon get(final long id){
        Session session = DataBaseConnection.getSessionFactory().openSession();
        try {
            session.clear();
            return session.get(Blazon.class, id);
        }finally {
            session.close();
        }
    }

    public Blazon get(final String description){
        Session session = DataBaseConnection.getSessionFactory().openSession();
        try {
            session.clear();
            List<Blazon> result = session.createSQLQuery("SELECT sourceId FROM blazonSource WHERE `description` = :val")
                    .setParameter("val", description)
                    .list();
            if (result.size() > 1){
                throw new IllegalArgumentException("Got more then one blazon");
            }

            return result.get(0);
        }finally {
            session.close();
        }
    }

    public void saveSourceId(final long blazonId, final String sourceId) throws SaveBlazonException {
        Session session = DataBaseConnection.getSessionFactory().openSession();
        try{
            session.clear();
            if (getSourceIds(blazonId).contains(sourceId)){
                return;
            }

            session.beginTransaction();

            session.createSQLQuery("INSERT INTO blazonSource SET blazonId= :id, sourceId= :val")
                    .setParameter("id", blazonId)
                    .setParameter("val", sourceId)
                    .executeUpdate();

            session.getTransaction().commit();
        }catch(Exception e){
            if(session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            throw new SaveBlazonException("Fail saving sourceId ("+e.getMessage()+")");
        }finally{
            session.close();
        }
    }

    public List<String> getSourceIds(final long blazonId){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try {
            session.clear();
            return session.createSQLQuery("SELECT sourceId FROM blazonSource WHERE blazonId= :id")
                    .setParameter("id", blazonId)
                    .list();
        }finally {
            session.close();
        }
    }
}
