package com.landsoflords.blazondb.repository;

import com.landsoflords.blazondb.DataBaseConnection;
import com.landsoflords.blazondb.exception.SaveEmblemException;
import com.landsoflords.blazondb.repository.model.Emblem;
import lombok.Getter;
import org.hibernate.Session;

import java.util.List;

public class EmblemRepository {

    @Getter
    private Long idLastSavedEmblem;

    public void save(Emblem emblem) throws SaveEmblemException {
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            session.beginTransaction();
            Long id = (Long) session.save(emblem);
            session.getTransaction().commit();
            idLastSavedEmblem = id;
        }catch(Exception e){
            if(session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            throw new SaveEmblemException(e.getMessage());
        }finally{
            session.close();
        }
    }

    public List<Emblem> get(){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            return session.createQuery("FROM Emblem")
                                        .list();
        }finally{
            session.close();
        }
    }

    public Emblem get(Long id){
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            return session.get(Emblem.class, id);
        }finally{
            session.close();
        }
    }

    public Emblem get(String description) throws Exception {
        Session session = DataBaseConnection.getSessionFactory().openSession();

        try{
            session.clear();
            List<Emblem> emblems = session.createQuery("FROM Emblem WHERE description= :val")
                    .setParameter("val", description)
                    .list();

            if (emblems.size() > 1){
                throw new Exception("More then one emblem");
            }

            return emblems.isEmpty() ? null : emblems.get(0);
        }finally{
            session.close();
        }
    }

    public void saveTags(final long emblemId, final List<String> tags) throws SaveEmblemException {
        Session session = DataBaseConnection.getSessionFactory().openSession();
        try{
            session.clear();
            session.beginTransaction();

            StringBuilder sql = new StringBuilder("INSERT INTO emblemTag(blazonId, sourceId) VALUES ");
            tags.forEach(item -> sql.append("(").append(emblemId).append(", '").append(item).append("'),"));
            sql.deleteCharAt(sql.length()-1);

            session.createSQLQuery(sql.toString()).executeUpdate();

            session.getTransaction().commit();
        }catch(Exception e){
            if(session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            throw new SaveEmblemException("Fail save emblem tag ("+e.getMessage()+")");
        }finally{
            session.close();
        }
    }
}
