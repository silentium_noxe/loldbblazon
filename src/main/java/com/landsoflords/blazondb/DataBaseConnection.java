package com.landsoflords.blazondb;

import com.landsoflords.blazondb.repository.model.Blazon;
import com.landsoflords.blazondb.repository.model.Color;
import com.landsoflords.blazondb.repository.model.Emblem;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class DataBaseConnection {

    private static final SessionFactory sessionFactory = buildSessionFactory();
    private static StandardServiceRegistry serviceRegistry;

    private static SessionFactory buildSessionFactory() {
        try {
            // Create session from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            configuration.configure();
            configuration.addAnnotatedClass(Blazon.class);
            configuration.addAnnotatedClass(Color.class);
            configuration.addAnnotatedClass(Emblem.class);
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

            return configuration.buildSessionFactory(serviceRegistry);
        }
        catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Cleaning cache and close connection with DB
        getSessionFactory().close();
    }
}
