package com.landsoflords.blazondb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PreDestroy;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PreDestroy
    public void destroy(){
        DataBaseConnection.shutdown();
        System.out.println("Connection with database was closed");
    }
}