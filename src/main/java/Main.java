import com.landsoflords.blazondb.repository.BlazonRepository;
import com.landsoflords.blazondb.repository.ColorRepository;
import com.landsoflords.blazondb.repository.EmblemRepository;
import com.landsoflords.blazondb.repository.model.Blazon;
import com.landsoflords.blazondb.repository.model.Emblem;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Map;

public class Main {

    public static void main(final String[] args) throws Exception {
        BlazonRepository blazonRepository = new BlazonRepository();

//        blazonRepository.save(blazon);
        System.out.println(blazonRepository.get(6));
    }
}