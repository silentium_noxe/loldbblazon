-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: lolDbBlazon
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blazon`
--

DROP TABLE IF EXISTS `blazon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blazon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backgroundStyle` varchar(64) DEFAULT NULL,
  `description` varchar(128) NOT NULL,
  `code` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blazon`
--

LOCK TABLES `blazon` WRITE;
/*!40000 ALTER TABLE `blazon` DISABLE KEYS */;
INSERT INTO `blazon` VALUES (16,'','Tenné, a snake Ermine','FmMklKGCpmJ0bQ__'),(17,'','Purpure, three snakes  Or','b2M7lqGCvoVofGd0'),(18,'per bend sinister','per bend sinister Azure and Or, I three books open  Argent, II three snakes  Sanguine','TGM71~dEQ1-ooZukv2TIfFWcn6-BvpQ~zjph'),(19,'quarterly','quarterly Or and Purpure, I IV two books open  Sable, II III a snake Or','HGM~UnpcPGKmoZukv2Rc5lean6-BJnMzWA__'),(20,'plain','Argent plain','gcs~Y2s_');
/*!40000 ALTER TABLE `blazon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blazonColor`
--

DROP TABLE IF EXISTS `blazonColor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blazonColor` (
  `blazonId` int(11) DEFAULT NULL,
  `colorId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blazonColor`
--

LOCK TABLES `blazonColor` WRITE;
/*!40000 ALTER TABLE `blazonColor` DISABLE KEYS */;
INSERT INTO `blazonColor` VALUES (16,8),(17,10),(18,6),(18,11),(19,10),(19,11),(20,7);
/*!40000 ALTER TABLE `blazonColor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blazonEmblem`
--

DROP TABLE IF EXISTS `blazonEmblem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blazonEmblem` (
  `blazonId` int(11) DEFAULT NULL,
  `emblemId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blazonEmblem`
--

LOCK TABLES `blazonEmblem` WRITE;
/*!40000 ALTER TABLE `blazonEmblem` DISABLE KEYS */;
INSERT INTO `blazonEmblem` VALUES (14,1),(17,2),(18,3),(18,4),(19,5),(19,6);
/*!40000 ALTER TABLE `blazonEmblem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blazonSource`
--

DROP TABLE IF EXISTS `blazonSource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blazonSource` (
  `blazonId` int(11) DEFAULT NULL,
  `sourceId` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blazonSource`
--

LOCK TABLES `blazonSource` WRITE;
/*!40000 ALTER TABLE `blazonSource` DISABLE KEYS */;
INSERT INTO `blazonSource` VALUES (14,'o251142'),(16,'f62485'),(17,'f62485'),(18,'o253470'),(19,'o253470'),(20,'f61176');
/*!40000 ALTER TABLE `blazonSource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (7,'Argent'),(6,'Azure'),(9,'Ermine'),(11,'Or'),(10,'Purpure'),(13,'Sable'),(12,'Sanguine'),(8,'Tenné');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emblem`
--

DROP TABLE IF EXISTS `emblem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emblem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `colorId` int(11) DEFAULT NULL,
  `quantity` varchar(16) DEFAULT NULL,
  `description` varchar(128) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emblem`
--

LOCK TABLES `emblem` WRITE;
/*!40000 ALTER TABLE `emblem` DISABLE KEYS */;
INSERT INTO `emblem` VALUES (2,11,'three','three snakes  Or',NULL),(3,7,'three','I three books open  Argent',NULL),(4,12,'three','II three snakes  Sanguine',NULL),(5,13,'two','I IV two books open  Sable',NULL),(6,11,NULL,'II III a snake Or',NULL);
/*!40000 ALTER TABLE `emblem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emblemTag`
--

DROP TABLE IF EXISTS `emblemTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emblemTag` (
  `emblemId` int(11) DEFAULT NULL,
  `tag` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emblemTag`
--

LOCK TABLES `emblemTag` WRITE;
/*!40000 ALTER TABLE `emblemTag` DISABLE KEYS */;
INSERT INTO `emblemTag` VALUES (2,'snake'),(3,'book'),(3,'open'),(4,'snake'),(5,'book'),(5,'open'),(6,'snake');
/*!40000 ALTER TABLE `emblemTag` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-18 17:22:27
